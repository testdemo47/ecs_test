//
//  ApiUnitTesting.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation

class ApiUnitTesting {
    
    
    func callImageDownloading(completionHandler: @escaping(_ status: Bool) -> Void) {
        
        NetworkUnitTesting.imageAvailable { (downloaded) in
            if downloaded {
                debugPrint("----> loaded Data")
                completionHandler(true)
            }else{
                completionHandler(false)
            }
        }
    }
    
    func callLocalJsonLoaded(completionHandler: @escaping(_ status: Bool) -> Void) {
        
        let baseURL = "https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=eaqRDHaEogFTIXmdYs7CIUHzlfPzjxJo"
        let url = URL.init(string: baseURL)
        NetworkUnitTesting.requestJsonLoadLocal(url: url!) { (loaded) in
            if loaded {
                debugPrint("----> loaded Data")
                completionHandler(true)
            }else{
                completionHandler(false)
            }
        }
    }
    
    
}
