//
//  NetworkUnitTesting.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation
import Alamofire

class NetworkUnitTesting {
    
    
    static func imageAvailable(complete: @escaping (_ result: Bool) -> Void) {
        
        let imageURL = URL.init(string: "https://static01.nyt.com/images/2019/10/24/us/24dc-durham/merlin_163189776_f0d3f5f1-028a-43de-8ad8-7cf2d23dace7-thumbStandard.jpg")!
        
        Alamofire.request(imageURL, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { (imgData) in
            do{
                if UIImage.init(data: imgData.data!) != nil{
                    complete(true)
                }else{
                    complete(false)
                }
            }
            
        }
    }
    
    static func requestJsonLoadLocal(url: URL, completion: @escaping (_ result: Bool) -> Void) {
        Alamofire.request(url).responseJSON { (json) in
            if let JSON = json.result.value {
                print("JSON: \(JSON)")
                completion(true)
            }else{
                completion(false)
            }
        }
        
    }
}
