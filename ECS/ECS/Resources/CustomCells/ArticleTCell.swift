//
//  ArticleTCell.swift
//  ECS
//
//  Created by Abdul on 10/30/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleTCell: UITableViewCell {

    @IBOutlet var imgArticle: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAuthor: UILabel!
    @IBOutlet var bthDate: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imgArticle.layer.cornerRadius = 25;
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(model : Article)  {
        
        self.lblName.text = model.title
        self.lblAuthor?.text = model.byline
        self.bthDate.setTitle( " " + model.published_date!, for: .normal)
        if let media = model.media.first {
            if let meta_data = media.media_metadata.first{
                let url = URL(string: meta_data.url)
                self.imgArticle.kf.indicatorType = .activity
                self.imgArticle.kf.setImage(with: url)
                }
            }
            
        }
}
