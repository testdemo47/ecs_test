//
//  APIRequest.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//
//eaqRDHaEogFTIXmdYs7CIUHzlfPzjxJo

import Foundation
import RxSwift


class APIRequest: NSObject {
    
    //    let url =
    
    let baseURL = "https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=eaqRDHaEogFTIXmdYs7CIUHzlfPzjxJo"
    var method = RequestType.GET
    var parameters = [String: String]()
    
    
    
    func request(with baseURL: URL) -> URLRequest {
        var request = URLRequest(url: baseURL)
        request.httpMethod = method.rawValue
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
    
    // create a method for calling api which is return a Observable
    func loadShows<T: Codable>(apiRequest: APIRequest) -> Observable<T> {
        return Observable<T>.create { observer in
            let request = apiRequest.request(with: URL.init(string: self.baseURL)!)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let model: ArticlesResults = try JSONDecoder().decode(ArticlesResults.self, from: data ?? Data())
                    observer.onNext( model.results as! T)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()
            
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
}
