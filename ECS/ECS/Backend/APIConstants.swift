//
//  APIConstants.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation

public enum RequestType: String {
    case GET, POST, PUT,DELETE
}

public enum Errors{
    case internetError(String)
    case serverError(String)
    case parsingError(String)
}
