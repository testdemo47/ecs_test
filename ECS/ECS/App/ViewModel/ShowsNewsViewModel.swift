//
//  ShowsNewsViewModel.swift
//  ECS
//
//  Created by Abdul on 10/30/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation
import RxSwift

protocol ShowsNewsViewModelProtocol: class {
    /// The corresponding borders
    var tvShows: Observable<[Article]> { get }
    var searchShow : Variable<String> { get }
    var filterShows : Variable<[Article]> { get }
}


class ShowsNewsViewModel : ShowsNewsViewModelProtocol{
    
    var tvShows: Observable<[Article]>
    
    var searchShow: Variable<String> = Variable("")
    var filterShows : Variable<[Article]> = Variable([])
    
    public let loading      : PublishSubject<Bool> = PublishSubject()
    
    lazy var searchShowObserable : Observable<String> = self.searchShow.asObservable()
    lazy var filteredShowsObserable : Observable<[Article]> = self.filterShows.asObservable()
    
    
    private let apiCalling = APIRequest()
    private let disposable = DisposeBag()
    
    init() {
        
        let client = self.apiCalling
        
        tvShows = client.loadShows(apiRequest: client).observeOn(MainScheduler.instance).share(replay: 1)
        
        
        searchShowObserable.subscribe(onNext: { (value) in
            
            self.tvShows.map({ $0.filter({
                if value.isEmpty { return true}
                return ($0.title.lowercased().contains(value.lowercased()) || $0.adx_keywords.lowercased().contains(value.lowercased()))
            })
            }).bind(to: self.filterShows)
                .disposed(by: self.disposable)
            
        }).disposed(by: disposable)
        
    }
    
    
}
