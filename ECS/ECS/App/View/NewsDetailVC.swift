//
//  NewsDetailVC.swift
//  ECS
//
//  Created by Abdul on 10/30/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import UIKit

class NewsDetailVC: UIViewController {

    @IBOutlet var imgArticle: UIImageView!
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblAuthor: UILabel!
    @IBOutlet var bthDate: UIButton!
    @IBOutlet var lblCaption: UILabel!
    @IBOutlet var lblAdx: UILabel!
    @IBOutlet var viewDetail: UIView!
    
    var objArticle : Article?  = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUI();
        // Do any additional setup after loading the view.
    }
    

    @IBAction func btnClose(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension NewsDetailVC{
    func setUI() {
        view.isOpaque = false
        view.backgroundColor = .clear //
        
        self.viewDetail.layer.cornerRadius = 25;
        self.viewDetail.borderWidth = 1
        self.viewDetail.borderColor = AppColor.appSecondaryColor

        
        self.lblName.text = self.objArticle!.title
        self.lblAuthor?.text = self.objArticle!.byline
        self.lblAdx?.text = self.objArticle!.adx_keywords
        self.lblCaption?.text = self.objArticle!.abstract

        self.bthDate.setTitle( " " + self.objArticle!.published_date!, for: .normal)
        if let media = self.objArticle!.media.first {
            if let meta_data = media.media_metadata.first{
                let url = URL(string: meta_data.url)
                self.imgArticle.kf.indicatorType = .activity
                self.imgArticle.kf.setImage(with: url)
            }
        }
        
    }
}
