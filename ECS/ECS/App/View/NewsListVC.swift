//
//  NewsListVC.swift
//  ECS
//
//  Created by Abdul on 10/30/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources
import MessageUI

class NewsListVC: BaseVC {
    
    @IBOutlet var tableNews: UITableView!
    
    private let apiCalling = APIRequest()
    private let disposeBag = DisposeBag()
    
    var viewModel = ShowsNewsViewModel()
    
    lazy var searchController : UISearchController = ({
        let controller = UISearchController(searchResultsController: nil)
        controller.dimsBackgroundDuringPresentation = false
        controller.searchBar.barTintColor = AppColor.appPrimaryColor
        controller.searchBar.tintColor = AppColor.appSecondaryColor
        controller.searchBar.placeholder = "Search News ......"
        return controller
    })()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setUI()
        setup()
    }
    
    
    override func showSearch()  {
        tableNews.setContentOffset(CGPoint.init(x: 0, y: 0), animated: true)
    }
    
    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
        if let vc = segue.destination as? NewsDetailVC, let detailToSend = sender as? Article {
            vc.modalPresentationStyle = .overCurrentContext
            vc.objArticle = detailToSend
        }
     }
 
    
}

//MARK: - Private
extension NewsListVC {
    
    func setup() {
        self.tableNews.isHidden = true;
        self.addNavLeftItems()
        self.addNavRightItems()

        self.startLoading()
        // thread
        DispatchQueue.main.asyncAfter(deadline: .now() + 5.0, execute: {
            self.bindTableView();
            self.bindSeachbar()
            self.setUI()
        })
    }
    
    func setUI()  {
        let searchBar = searchController.searchBar
        tableNews.tableHeaderView = searchBar
        tableNews.contentOffset = CGPoint.init(x: 0, y: searchBar.frame.size.height)
        
        self.navigationController?.navigationBar.layer.shadowColor = UIColor.gray.cgColor
        self.navigationController?.navigationBar.layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        self.navigationController?.navigationBar.layer.shadowRadius = 4.0
        self.navigationController?.navigationBar.layer.shadowOpacity = 1.0
        self.navigationController?.navigationBar.layer.masksToBounds = false
    }
    
    
    func bindTableView() {
        
        
        viewModel.filterShows.asObservable().bind(to: tableNews.rx.items){(tv, row, item) -> UITableViewCell in
            self.endLoading()
            self.tableNews.isHidden = false;
            let cell = tv.dequeueReusableCell(withIdentifier: "ArticleTCell", for: IndexPath.init(row: row, section: 0)) as! ArticleTCell
            
            cell.configure(model : item)
            
            return cell
            
            
            }.disposed(by: disposeBag)
        
        tableNews.rx.itemSelected.subscribe(onNext: { IndexPath in
            self.tableNews.deselectRow(at: IndexPath, animated: true)
        }).disposed(by: disposeBag)
        
        
        tableNews.rx.modelSelected(Article.self)
            .subscribe(onNext: { (item) in
                print(item.title)
                self.openDetail(object: item)
            }).disposed(by: disposeBag)
    }
    
    
    
    
    func bindSeachbar() {
        searchController.searchBar.rx.text
            .orEmpty
            .distinctUntilChanged()
            .bind(to: viewModel.searchShow)
            .disposed(by: disposeBag)
    }
    
}

extension NewsListVC : MFMailComposeViewControllerDelegate{
    func openDetail(object : Article) {
        DispatchQueue.main.async(){
            self.performSegue(withIdentifier: "segueDetail", sender: object)
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue:
            print("Cancelled")
        case MFMailComposeResult.saved.rawValue:
            print("Saved")
        case MFMailComposeResult.sent.rawValue:
            print("Sent")
        case MFMailComposeResult.failed.rawValue:
            print("Error: \(String(describing: error?.localizedDescription))")
        default:
            break
        }
        controller.dismiss(animated: true, completion: nil)    }
}
