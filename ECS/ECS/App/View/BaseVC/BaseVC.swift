//
//  BaseVC.swift
//  ECS
//
//  Created by Abdul on 10/30/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import UIKit

open class BaseVC: UIViewController {
    
    @objc func showSearch(){};
    @objc func showOptions(){};
    @objc func showMenu(){};

    
    public func addNavRightItems() {
        let searchBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .search, target: self, action: #selector(showSearch))
        let optionsBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "icon_options"), style: .plain, target: self, action: #selector(showMenu))
        self.navigationItem.rightBarButtonItems  = [optionsBarButtonItem,searchBarButtonItem]
    }
    
    public func addNavLeftItems() {
        
        let menuBarButtonItem = UIBarButtonItem.init(image: UIImage.init(named: "icon_menu"), style: .plain, target: self, action: #selector(showMenu))

        self.navigationItem.leftBarButtonItem  = menuBarButtonItem
    }
    
}
