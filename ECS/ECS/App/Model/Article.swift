//
//  Article.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation

struct Article: Codable {
    
//    let url: String
    let adx_keywords: String
//    let column: String?
//    let section: String
    let byline: String
//    let type: String?
    let title: String
    let abstract: String
    let published_date: String?
    let source: String
//    let per_facet: String?
//    let id: u_long
//    let asset_id: u_long
//    let views: Int
//    let des_facet: [String]
//    let org_facet: [String]
//    let geo_facet: [String]
    let media: [MediaObj]
//    let uri: String
    
    private enum CodingKeys: String, CodingKey {
//        case url
        case adx_keywords
//        case column
//        case section
        case byline
//        case type
        case title
        case abstract
        case published_date
        case source
//        case per_facet
//        case id
//        case asset_id
//        case views
//        case des_facet
//        case org_facet
//        case geo_facet
        case media
//        case uri
    }
}
