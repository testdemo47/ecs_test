//
//  ArticlesResults.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation

struct ArticlesResults: Codable {
    
    let status: String
    let copyright: String
    let results: [Article]
    let num_results: u_long

    private enum CodingKeys: String, CodingKey {
        case status
        case copyright
        case results
        case num_results
    }
}

