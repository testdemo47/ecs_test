//
//  MetaData.swift
//  ECS
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import Foundation

struct MetaData: Codable {
    

    let url: String!
    let format: String!
    let height: Int
    let width: Int
    
    private enum CodingKeys: String, CodingKey {
        case url
        case format
        case height
        case width
    }
}


