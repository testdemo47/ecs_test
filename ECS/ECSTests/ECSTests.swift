//
//  ECSTests.swift
//  ECSTests
//
//  Created by Abdul on 10/29/19.
//  Copyright © 2019 Abdul. All rights reserved.
//

import XCTest
@testable import ECS

class ECSTests: XCTestCase {

    let vc = ApiUnitTesting()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testImgProfile(){
        let e = expectation(description: "Image Downloaded")
        vc.callImageDownloading { (isFinished: Bool) in
            if isFinished {
                debugPrint("Finished in unit test!!!")
                let expectedString = true
                XCTAssertEqual(isFinished, expectedString)
            }else{
                XCTFail()
            }
            e.fulfill()
        }
        waitForExpectations(timeout: 5.0, handler: nil)
    }
    
    func testLocalJsonLoaded(){
        let e = expectation(description: "Local Json File Downloaded")
        vc.callLocalJsonLoaded { (isLoaded : Bool) in
            if isLoaded{
                debugPrint("Finished in unit test!!!")
                let expectedString = true
                XCTAssertEqual(isLoaded, expectedString)
            }else{
                XCTFail()
            }
            e.fulfill()
        }
        waitForExpectations(timeout: 2.0, handler: nil)
        
    }
}
